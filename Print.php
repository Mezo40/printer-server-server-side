<?php
/*
** Print.php
** Providing Server Side Printing Interface for `Printer Server`
**
** AUTHOR  : Mohammed Elghamry <elghmary_work@outlook.com>
** CREATED : 26/10/2018 05:50 PM
*/

namespace PrinterServer;

require_once "ErrorLog.php";
require_once "PrintObjects/PrintJob.php";
require_once "PrintObjects/PrintJobOrder.php";
require_once "PrintObjects/PrintJobFoodItem.php";

/**
 * Printer Server Static Main Class
 */
class PrinterServer
{
    const SQLITE_DB_PATH            = 'queue/queue.db';
    const TOKEN_LENGTH              = 16;
    const TOKEN_BYTE_LENGTH         = self::TOKEN_LENGTH / 2;

    /**
     * Default structure of the db
     */
    const QUEUE_SQLITE_STRUCTURE    = <<<'SQL'
CREATE TABLE orders_queue (
    id            INTEGER PRIMARY KEY AUTOINCREMENT,
    time_created  TEXT DEFAULT CURRENT_TIMESTAMP,
    token         CHARACTER(16) UNIQUE NOT NULL,
    serialized    TEXT NOT NULL
);
SQL;

    /**
     * SQLite queue file
     *
     * @var \PDO
     */
    private static $queueSQLite = null;

    // Class is static and cannot be instantiated
    private function __construct()
    {
    }

    /**
     * Connect to the db
     *
     * @return bool
     */
    private static function connect()
    {
        $createNew_flag = false;
        // Check of Presence
        if (!file_exists(self::SQLITE_DB_PATH)) {
            $createNew_flag = true;
        }

        try {
            // Connect to the db
            self::$queueSQLite = new \PDO('sqlite:' . self::SQLITE_DB_PATH);

            // Create the db if not exists
            if ($createNew_flag) {
                if (!self::$queueSQLite->query(self::QUEUE_SQLITE_STRUCTURE)) {
                    self::$queueSQLite = null;
                    throw new \Exception("Error Creating SQLite DB");
                }
            }
        } catch (\Exception $ex) {
            self::$queueSQLite = null;
            // Log
            ErrorLog::log("Cannot Connect to sqlite db!\n" . $ex->getMessage());
            // Error
            return false;
        }
        // Done
        return true;
    }

    /**
     * Generate Token for the queue entry
     *
     * @return string
     */
    private static function generateToken()
    {
        $randBytes = random_bytes(self::TOKEN_BYTE_LENGTH);
        return bin2hex($randBytes);
    }

    /**
     * Add a new job to the queue
     *
     * @param \PrinterServer\PrintJob $printJob
     * @return bool
     */
    public static function addJobToQueue(\PrinterServer\PrintJob $printJob)
    {
        if (!$printJob) {
            return false;
        }
        if (!self::$queueSQLite && !self::connect()) {
            return false;
        }

        $printJobToken =  self::generateToken();

        // Set order token
        $reflection = new \ReflectionClass('PrinterServer\PrintJob');
        $property = $reflection->getProperty('JobToken');
        $property->setAccessible(true);
        $property->setValue($printJob, $printJobToken);

        // Serialize
        $serializedData = $printJob->serialize();

        // Do SQL Query
        $sql = <<<'SQL'
INSERT INTO orders_queue(token, serialized)
VALUES (?,?)
SQL;
        try {
            if ($stmt = self::$queueSQLite->prepare($sql)) {
                $stmt->bindParam(1, $printJobToken);
                $stmt->bindParam(2, $serializedData);
                if ($stmt->execute()) {
                    // Done
                    return true;
                }
            }
        } catch (\Exception $ex) {
            // Log
            ErrorLog::log("Couldn't addJobToQueue!\n" . $ex->getMessage());
        }
        // Not done
        return false;
    }

    /**
     * Remove Job From Queue
     *
     * @param string $token
     * @return bool
     */
    public static function removeJobFromQueue(string $token)
    {
        if (empty($token)) {
            return false;
        }
        if (!self::$queueSQLite && !self::connect()) {
            return false;
        }

        // Set SQL
        $sql = <<<'SQL'
DELETE FROM orders_queue
WHERE token=?
SQL;
        try {
            if ($stmt = self::$queueSQLite->prepare($sql)) {
                $stmt->bindParam(1, $token);
                if ($stmt->execute()) {
                    // Done
                    return true;
                }
            }
        } catch (\Exception $ex) {
            // Error and log
            ErrorLog::log("Couldn't removeJobFromQueue!\n" . $ex->getMessage());
        }
        // Not done
        return false;
    }

    /**
     * Get all Jobs
     *
     * @return array
     */
    public static function getAllJobs()
    {
        if (!self::$queueSQLite && !self::connect()) {
            return null;
        }

        $sql = <<<'SQL'
SELECT serialized FROM orders_queue;
SQL;
        try {
            if ($stmt = self::$queueSQLite->query($sql)) {
                $dataArray = $stmt->fetchAll(\PDO::FETCH_COLUMN);
                if ($dataArray !== false) {
                    return $dataArray;
                }
            }
        } catch (\Exception $ex) {
            // Error and log
            ErrorLog::log("Couldn't getAllJobs!\n" . $ex->getMessage());
        }
        return null;
    }
}
