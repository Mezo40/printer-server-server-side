<?php
/*
** PrintAccept.php
** Accept a print job to be removed from the queue
**
** AUTHOR  : Mohammed Elghamry <elghmary_work@outlook.com>
** CREATED : 27/10/2018 01:51 AM
*/

require_once "Print.php";

// Check the request
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['token'])) {
    // Get the token
    $token = $_GET['token'];

    // Remove
    if (\PrinterServer\PrinterServer::removeJobFromQueue($token)) {
        echo "SUCCESS";
    } else {
        echo "FAILED";
    }
} else {
    echo "FAILED";
}
